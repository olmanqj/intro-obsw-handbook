El Capitulo 1, expondrá brevemente todos los conceptos básicos que se deben conocer antes de leer el resto del manual. Si usted ya tiene conocimientos de la estructura y partes de un satélite, misiones satelitales, etc siéntase libre de saltar esta sección.

El Capitulo 2, consiste en una breve reseña histórica acera de computadoras y software a bordo de satélites.

Finalmente el Capitulo 3, concite en el segmento introductorio del manual, es decir una breve introduction al software a bordo de satélites.

