# Introducción

## Computadora a bordo

Si se encuentra leyendo este material, es probable que es de su conocimiento que todo satélite artificial contiene en su interior una computadora, llamada computadora abordo, o en ingles, on-board computer (OBC). Este componente es uno de los más importantes del satélite, usualmente se le llama el 'cerebro' de la nave espacial, dado el rol que desempeña.

La OBC, como cualquier otra computadora, debe ser programada durante la construcción del satélite. El software que se ejecuta en la OBC se le llama On-Board Software (OBSW), o, Flight Software (FS).

Una aclaración importante, aunque OBC y OBSW son elementos completamente distintos, están fuertemente entrelazados (obviamente, el software necesita de una computadora para ejecutarse, y una computadora sin software es un objeto inanimado).

Spoiler alert: Por lo general la OBC no constituye el unico procesador en el satelite. Es comun que los distintos subsitemas tengan su propio controlador, ejecutando software distinto y especializado para las funciones previstas.Como por ejemplo, los radio-transmisores digitales puden tener un procesador que recive y envia paquetes de comunicacion. El Subsitema de Potencia puede tener un procesador para regular la carga y consumo de poder. El Subsistema de Control de la Orientacion tambien puede tener un procesador para ejecutar los algoritmos de control. Si este es el caso, las computadoras de cada subsytema se comunicaran de alguna manetacon la OBC, ya que es esta quien comanda cada subsitema  para conducir exitosamente la mision. En el resto de este documento nos centraremos especificamente en el softwae de la OBC y no del resto de los subsitemas, sin embargo pueden existir amplias similitudes.

La pregunta del millón es entonces. ¿Como programar una OBC? En otras palabras. ¿Como programar un satélite? Ese es el tema principal que pretende abordar este documento. La respuesta corta: no hay respuesta corta. Mejor dicho, no hay una respuesta única. A lo largo de este documento vamos a ver que no existe una recete, o una, guía definitiva. Cada satélite/misión es único y diferente, cada cual tiene sus propios requerimientos y limitaciones. Sin embargo, también veremos que existen ciertas funciones (o más bien tareas) que son esenciales para el  correcto desarrollo de prácticamente cualquier misión, y por lo tanto están presentes en todo satélite.


De vuelta al tema principal: Como programar un satélite? Podemos descomponer esta pregunta en varias preguntas más específicas:

- ¿Qué funciones debe desempeñar la OBC? - Requerimientos de software
- ¿Cómo estructurar el OBSW? - Diseño, Arquitectura y capas
- ¿Cómo podemos comunicarnos con el OBC? - Interacción, protocolos y red
- ¿Cómo podemos comunicarnos con otros subsistemas? - Interfaces de bajo nivel
- ¿Cómo se trasmite la información hacia y desde el satélite? - Serialización
- ¿Como monitorear y saber el estado de nuestro satélite? - Telemetría
- ¿Cómo podemos controlar el satélite? - Telecomandos
- ¿Qué tecnologías y procesos se utilizan para desarrollar el OBSW? - Ambiente de desarrollo
- ¿Como asegurarnos de que el OBSW va a funcionar? - Aseguramiento de la calidad
- ¿Debe nuestro OBSW cumplir con regulaciones? - Estándares y leyes


En lo que queda de este documento vamos a tratar de responder cada una de estas preguntas. 


## Funciones del OBSW

¿Qué funciones debe desempeñar la OBC? La respuesta a esta pregunta es la más relativa de todas las cuestiones tratadas en este documento. Es también la razón por la que no hay una respuesta definitiva a como programas un satélite. Cuáles son todas y cada una de las funciones que debe cumplir un satélite, y por lo tanto la OBC, depende completamente de la misión. Es decir el objetivo de la misión es lo que va a dictar las funciones o tareas que debe realizar la OBC, y por lo tanto el OBSW es único para cada misión.

Existen un sinfín de tipos de misiones espaciales, solo por nombrar algunos: comunicaciones, observación terrestre, telescopios astronómicos, laboratorios espaciales, observación climatológica, cápsulas de transporte, navegación terrestre, etc, etc, etc.

Ahora bien, si existe una OBC en todo satélite es por algo, su papel es claro y especifico. Podemos dividir sus funciones en dos grandes ramas: 

- **Servir de interfaz entre estaciones terrestres y el satélite**: ser el componente que entiende los mensajes que se envían desde tierra y poder interpretarlos, por lo tanto brindar a los operadores de la misión un medio para controlar el satélite y cada uno de los subsistemas. A su vez, ser el componente que envía información del estado del satélite y cada uno de los subsistemas a tierra, con el fin de que los operadores conozcan el estado de la nave espacial.
- **Coordinar el funcionamiento de los subsistemas**: ser el componente que tiene la capacidad de comandar cada uno de los subsistemas, para que de esta manera el satélite pueda desempeñar su tarea autónomamente. Al mismo tiempo, ser el componente que monitorea periódicamente el estado y correcto funcionamiento del resto de los subsistemas, y en el caso dado de una falla, llevar acabo acciones correctivas para asegurar la misión. 

Lo anterior nos revela algo crucial, la OBC, no es nada más y nada menos que el flujo de información. Todo se resume en datos que van y vienen desde y hacia estaciones en tierra, el satélite, y cada componente. Y en realidad, esta es la razón de toda misión espacial no tripulada: datos/información; valiosa y preciada información. El Rover Curiosity ha de ser una de las invenciones más impresionantes de la humanidad, su vida se resume en enviar información desde Marte hacia La Tierra.

Si nos vamos a la literatura [1], al subsistema satelital que se encarga de todo lo referente al manejo de información se le llama Command and Data Handling Subsystem (CDHS), en español Subsistema de Comandos y Manejo de Datos. La OBC es el elemento central de este subsistema. Podremos encontrar que al CDHS se le atribuyen las siguientes tareas:

- Controlar la orientacion y orbita.
- Controlar el consumo de poder y carga.
- Controlar el payload e instrrumentos scientificos.
- Monitorear el estado del systema.
- Detectar, aislar y corregirr fallas.
- Ejecutar los comandos provenientes de estaciones terrestres.
- Preparar información para ser enviada a estaciones terrestres.
- Controlar el consumo de potencia y carga de las baterías por medio de los paneles solares.
- Controlar la temperatura interna del satélite por medio de calentadores y radiadores.
- Recolectar y almacenar información del resto de subsistemas y el payload.
- Mantener y distribuir el tiempo (fecha/hora).
- Calcular la posición y ubicación de la nave espacial en su órbita.
- Desplegar antenas/sensores una vez en órbita.
- Comandar maniobras de navegación y orientación a actuadores (motores, propulsores, etc.).
- Monitorear autónomamente el estado y llevar a cabo acciones correctivas en caso de eventos inesperados.


Lo anterior son solo ejemplos de algunas de las tareas que podría desempeñar la OBC, sin embargo no todas están presentes en toda misión espacial, por ejemplo un satélite sin control activo de la orientación, no tendrá que ejecutar maniobras de navegación y orientación. Por otro lado, hay tareas que si están presentes en todo satélite, por ejemplo ejecutar comandos provenientes de estaciones terrestres, o, preparar información para ser enviada a estaciones terrestres.
