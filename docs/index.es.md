# Inicio

Bienvenid@ a **[_Una introducción al software de satélites_]()**.

El presente manual tiene como objetivo principal brindar una introducción intuitiva y progresiva al tema de On-boards software para satélites.  Esta dirigido a estudiantes y/o cualquier profesional que cuenten con algún conocimiento en software, sistemas embebidos, y/o misiones espaciales, pero ningún conocimiento en el desarrollo de software para naves espaciales.

Importante aclarar que el principal enfoque de este documento son los pequeños satélites (por ejemplo los CubeSats). Y no los grandes satélites, como los típicos satélites creados por agencias espaciales nacionales (ejemplo: NASA, ESA).

## Contenido

Parte 1 - Introducción

1. Glosario
1. Reseña histórica
1. Introducción

Parte 2 - Arquitectura

1. Arquitectura del OBSW
1. Capas de software
1. Sistema Operativo

Parte 3 - Comunicaciones

1. Interacciones
1. Protocolos
1. Interfaces
1. Procesamiento de datos

Parte 4 - Conceptos espaciales

1. Telemetria
1. Comandos
1. Operaciones y Procedimientos
1. Manejo de fallas


Parte 5 - Proceso de desarrollo

1. Proceso de desarrollo
1. Requerimientos de software
1. Tecnologias
1. Verificación
1. Estándares

Parte 6 - Futuro y principales retos

1. El futuro del OBSW
1. Principales retos

---

## Sobre el autor

[Olman Quiros Jimenez](http://www.olman.me/)